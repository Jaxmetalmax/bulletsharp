# README #

This is Bullet Sharp, a Desktop App to use [Pushbullet](https://www.pushbullet.com) service 

You can push **Notes, Links, Addresses, Lists and Files** from this app.

### How do I get set up? ###

To compile this app on** Windows** you need the following packages:

* .NET Framework 4.0 or greater.

* VS 2010 or greater.
* Json.NET
* Microsoft HTTP Client Libraries

*You can get these packages via NuGet package manager (when compiling this, NuGet download the missing packages)*

To compile this app on **Linux and OSX** you need Mono Develop (Xamarin Studio) (Tested on Mono Develop 4.2.2)