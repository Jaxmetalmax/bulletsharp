﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.IO;
using System.Windows.Forms;

namespace BulletSharp
{
    class LogErrors
    {
        const string fileName = "ErrorLog.txt";

        static readonly string path = Path.GetDirectoryName(Application.ExecutablePath);

        static readonly string pathString = Path.Combine(path, fileName);

        public static void LogError(string errorLog)
        {

            if (!File.Exists(pathString)) //No File? Create
            {
                try
                {
                    using (var fs = new FileStream(pathString, FileMode.Create))
                    {
                        fs.Close();
                    }
                }
                catch (Exception)
                {
                    throw;
                }

            }

            if (File.ReadAllBytes(pathString).Length >= 100 * 1024 * 1024) // (100mB) File to big? Create new
            {
                try
                {
                    File.WriteAllText(pathString, String.Empty);
                }
                catch (Exception)
                {

                    throw;
                }

                using (var f = new StreamWriter(pathString, true))
                {
                    f.WriteLine("\r\n ======================");
                    f.WriteLine("Fecha hora: " + DateTime.Now.ToString());
                    f.WriteLine("\r\n ");
                    f.WriteLine(errorLog);
                    f.WriteLine("\r\n ======================");
                    f.Close();
                }
            }
            else
            {
                try
                {
                    using (var f = new StreamWriter(pathString, true))
                    {
                        f.WriteLine("\r\n ======================");
                        f.WriteLine("Fecha hora: " + DateTime.Now.ToString());
                        f.WriteLine("\r\n ");
                        f.WriteLine(errorLog);
                        f.WriteLine("\r\n ======================");
                        f.Close();
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
    }
}
