﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace BulletSharp
{
    public partial class frmReminders : Form
    {
        private readonly Dictionary<string, string> _devices;
        private string _idDev;
        private string _nameDev;

        public frmReminders(Dictionary<string,string> dDevices )
        {
            InitializeComponent();
            _devices = dDevices;
        }

        private void btnAddReminder_Click(object sender, EventArgs e)
        {
            if (txtReminder.Text.Length <= 0) return;
            var formatedtime = dtmReminder.Value.ToString("HH:mm");
            var date = dtmReminder.Value.ToShortDateString();
            var toBeSend = Convert.ToDateTime(date + " " + formatedtime);

            if (toBeSend < DateTime.Now.Subtract(new TimeSpan(0,0,3,0)))
            {
                MessageBox.Show("Hey! Wanna send a reminder to the past?¡","We need to go back!",
                    MessageBoxButtons.OK,MessageBoxIcon.Information);
            }

            Helper.CreateReminder(txtReminder.Text, date, formatedtime, _idDev, _nameDev);

            tsMessage.Text = "Reminder added to be sent on : "+date+" "+formatedtime;
            tsMessage.ForeColor = Color.Blue;
            loadReminders();

        }

        private void frmReminders_Load(object sender, EventArgs e)
        {
            tsMessage.Text = string.Empty;
            cmbDevices.DataSource = new BindingSource(_devices, null);
            cmbDevices.DisplayMember = "Value";
            cmbDevices.ValueMember = "Key";

            cmbDevices.SelectedItem = cmbDevices.Items[0];
            _idDev = cmbDevices.SelectedValue.ToString();
            _nameDev = cmbDevices.GetItemText(cmbDevices.SelectedItem);

            loadReminders();
        }

        private void loadReminders()
        {
            var bs = new BindingSource {DataSource = Helper.loadReminders()};
            dgReminders.DataSource = bs;
            dgReminders.Columns["Id"].Visible = false;
            dgReminders.Columns["Reminder"].HeaderText = "Reminder";
            dgReminders.Columns["Date"].HeaderText = "Date";
            dgReminders.Columns["Time"].HeaderText = "Time";
            dgReminders.Columns["Id Name"].HeaderText = "Device";
            dgReminders.Columns["Sent"].Visible = false;
            dgReminders.Columns["Id Device"].Visible = false;

            dgReminders.Refresh();
        }

        private void cmbDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            _idDev = cmbDevices.SelectedValue.ToString();
            _nameDev = cmbDevices.GetItemText(cmbDevices.SelectedItem);
        }
    }
}
