﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;

namespace BulletSharp
{
    static class Helper
    {
        public static void Createdb()
        {
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            var pathString = Path.Combine(path, "bulletreminders.db");

            if (!File.Exists(pathString))
            {
                SQLiteConnection.CreateFile("bulletreminders.db");

                var cnn = new SQLiteConnection("Data Source=bulletreminders.db");

                cnn.Open();

                const string sql = "CREATE TABLE reminders" +
                                   " (id INTEGER, reminder TEXT, date TEXT NOT NULL, " +
                                   "time TEXT NOT NULL, id_device TEXT NOT NULL, " +
                                   "name_device TEXT, sent TEXT,PRIMARY KEY (id));";

                var command = new SQLiteCommand(sql, cnn);
                command.ExecuteNonQuery();

                cnn.Close();
            }
        }


        public static int CreateReminder(string noteReminder, string date, string time, string idDev, string nameDev)
        {
            var cnn = new SQLiteConnection("Data Source=bulletreminders.db");

            cnn.Open();
            int rowsUpdated =0;
            
            var insertSql = new SQLiteCommand("INSERT INTO reminders (reminder, date, time, id_device,name_device) VALUES (@reminder,@date,@time,@idDev,@nameDev)", cnn);
            insertSql.Parameters.AddWithValue("@reminder", noteReminder);
            insertSql.Parameters.AddWithValue("@date",date);
            insertSql.Parameters.AddWithValue("time",time);
            insertSql.Parameters.AddWithValue("@idDev",idDev);
            insertSql.Parameters.AddWithValue("@nameDev", nameDev);
            try
            {
                rowsUpdated = insertSql.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups! Something went wrong when creating reminder...");
                var cError = ex.Message + "\r\n" + ex.StackTrace;
                LogErrors.LogError(cError);
                cnn.Close();
            } 

            return rowsUpdated;
        }

        public static DataTable loadReminders()
        {
            var cnn = new SQLiteConnection("Data Source=bulletreminders.db");

            cnn.Open();
            string SQL = "SELECT * FROM reminders WHERE sent='0'";
            var cmd = new SQLiteCommand(SQL);
            cmd.Connection = cnn;

            SQLiteDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();

            dt.Columns.Add("Id", typeof (int));
            dt.Columns.Add("Reminder", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("Id Device", typeof(string));
            dt.Columns.Add("Id Name", typeof(string));
            dt.Columns.Add("Sent", typeof(string));

            while (dr.Read())
            {
                dt.Rows.Add(dr.GetInt32(0), dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4),
                    dr.GetString(5), dr.GetString(6));
            }
            return dt;
        }

        public static void UpdateReminder(string id)
        {
            var cnn = new SQLiteConnection("Data Source=bulletreminders.db");

            cnn.Open();
            
            var updateSql = new SQLiteCommand("UPDATE reminders SET sent=@sent WHERE id=@id", cnn);
            updateSql.Parameters.AddWithValue("@sent", "1");
            updateSql.Parameters.AddWithValue("id", id);
            try
            {
                updateSql.ExecuteNonQuery();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups! Something went wrong when updating reminder...");
                var cError = ex.Message + "\r\n" + ex.StackTrace;
                LogErrors.LogError(cError);
                cnn.Close();
            }
        }
    }
}
