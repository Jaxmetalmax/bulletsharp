﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * This program makes use of JDI WebSocket Client that is under Apache 2.0 
   License.
   JDI WebSocket Client site: http://jdiwebsocketclient.codeplex.com/
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using JDI.WebSocket.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BulletSharp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        #region variables
        private static string _idDev;
        private static string _emailContact;
        private static bool _acceptCert;
        private static bool _sendToEmail;
        private static readonly string _version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        private static readonly string _build = "b20082015";
        private Dictionary<string, string> _devices;
        private Dictionary<string, string> _contacts;
        private DataTable _dtReminderes;
        private WebSocketClient webSocketClient;
        private static string _lastUpd = "0";
        private static List<string> _listNotif = new List<string>();
        private PropertiesJson properties = new PropertiesJson();
        private const string fileName = "prop.json";
        private static readonly string path = Path.GetDirectoryName(Application.ExecutablePath);
        private readonly string pathString = Path.Combine(path, fileName);

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            WSOptions wsOptions = new WSOptions();
            wsOptions.MaskingEnabled = true;
            wsOptions.ActivityTimerEnabled = true;

            // init websocket client
            this.webSocketClient = new WebSocketClient("websocket", wsOptions);
            this.webSocketClient.TextMessageReceived += new WSDelegates.TextMessageReceivedEventHandler(webSocketClient_TextMessageReceived);
            if (properties.ApiKey != "" && properties.EnableNotifications)
                webSocketClient.Connect("wss://stream.pushbullet.com/websocket/" + properties.ApiKey);
            
            base.OnLoad(e);
        }

        private void webSocketClient_TextMessageReceived(string message)
        {
            if (message != "{\"type\": \"nop\"}")
            {
                if (properties.EnableNotifications)
                readPushes();
            }
        }

        private void readPushes()
        {
            var jPushHistory = BulletAPI.BulletAPI.RequestPushHistory(properties.ApiKey, _acceptCert, _lastUpd);
            var jPushes = (JArray) jPushHistory["pushes"];
            var nFirst = 0;

            var title = string.Empty;

            foreach (var push in jPushes)
            {
                if (nFirst == 0) _lastUpd = push["modified"].ToString();
                nFirst++;

                if (push["active"].ToString() != "True") continue;
                if (push["dismissed"].ToString() != "False") continue;
                if (_listNotif.Contains(push["iden"].ToString()))continue;

                switch (push["type"].ToString())
                {
                    case "note":
                        title = push["title"].ToString() != ""
                            ? push["title"].ToString()
                            : "";
                        var message = push["body"].ToString();
                        notifyIcon1.BalloonTipTitle = "New Note";
                        notifyIcon1.BalloonTipText = title + "\r\n" + message;
                        notifyIcon1.ShowBalloonTip(20000);
                        _listNotif.Add(push["iden"].ToString());
                        break;
                    case "link":
                        title = push["title"].ToString() != ""
                            ? push["title"].ToString()
                            : "";
                        var url = push["url"].ToString();
                        notifyIcon1.BalloonTipTitle = "New Link";
                        notifyIcon1.BalloonTipText = title + "\r\n" + url;
                        notifyIcon1.ShowBalloonTip(20000);
                        _listNotif.Add(push["iden"].ToString());
                        break;
                    case "file":
                        var file_name = push["file_name"].ToString() != ""
                            ? push["file_name"].ToString()
                            : "";
                        var file_url = push["file_url"].ToString();
                        file_url = file_url.Substring(0, 30);
                        file_url = file_url + "...";
                        notifyIcon1.BalloonTipTitle = "New File";
                        notifyIcon1.BalloonTipText = file_name + "\r\n" + file_url;
                        notifyIcon1.ShowBalloonTip(20000);
                        _listNotif.Add(push["iden"].ToString());
                        break;
                }
                Thread.Sleep(10000);
            }
        }

        private void readPushesFirst()
        {
            var jPushHistory = BulletAPI.BulletAPI.RequestPushHistory(properties.ApiKey, _acceptCert, _lastUpd);
            var jPushes = (JArray)jPushHistory["pushes"];
            var nFirst = 0;

            foreach (var push in jPushes)
            {
                if (nFirst == 0) _lastUpd = push["modified"].ToString();
                if (push["active"].ToString() != "True") continue;
                if (push["dismissed"].ToString() != "False") continue;
                _listNotif.Add(push["iden"].ToString());
                if (nFirst == 0)
                {
                    var title = "";
                    nFirst++;
                    switch (push["type"].ToString())
                    {
                        case "note":
                            title = push["title"].ToString() != ""
                                ? push["title"].ToString()
                                : "";
                            var message = push["body"].ToString();
                            notifyIcon1.BalloonTipTitle = "New Note";
                            notifyIcon1.BalloonTipText = title + "\r\n" + message;
                            notifyIcon1.ShowBalloonTip(20000);

                            break;
                        case "link":
                            title = push["title"].ToString() != ""
                                ? push["title"].ToString()
                                : "";
                            var url = push["url"].ToString();
                            notifyIcon1.BalloonTipTitle = "New Link";
                            notifyIcon1.BalloonTipText = title + "\r\n" + url;
                            notifyIcon1.ShowBalloonTip(20000);
                            _listNotif.Add(push["iden"].ToString());
                            break;
                        case "file":
                            var file_name = push["file_name"].ToString() != ""
                                ? push["file_name"].ToString()
                                : "";
                            var file_url = push["file_url"].ToString();
                            file_url = file_url.Substring(0, 30);
                            file_url = file_url + "...";
                            notifyIcon1.BalloonTipTitle = "New File";
                            notifyIcon1.BalloonTipText = file_name + "\r\n" + file_url;
                            notifyIcon1.ShowBalloonTip(20000);
                            _listNotif.Add(push["iden"].ToString());
                            break;
                    }
                }
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            tsMessage.Text = string.Empty;

            try
            {
                properties =
                    (PropertiesJson)
                        JsonConvert.DeserializeObject(File.ReadAllText(pathString), typeof(PropertiesJson));
            }
            catch (Exception ex)
            {
                LogErrors.LogError(ex.Message + ex.StackTrace);
                properties = new PropertiesJson();
            }

            if (LoadDevices())
            {
                Helper.Createdb();

                EnablePush();
                enableReminders();

                if (properties.EnableNotifications)
                { readPushesFirst(); }
                
            }
            else
            {
                manageRemindersToolStripMenuItem.Enabled = false;
            }
            Cursor.Current = Cursors.Default;
        }
    
        private bool LoadDevices()
        {
            bool isReloaded = false;

            _acceptCert = properties.AcceptCerts;
            //_acceptCert = Settings.Default.AcceptCerts;
            

            DisablePush();
            //if (Settings.Default.ApiKey.Length <= 0)
            if (properties.ApiKey.Length <= 0)
            {
                if (DialogResult.OK == MessageBox.Show("Token Key not loaded, please set your Token Key on settings.", "Token Key not loaded",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
                    using (var frmSet = new frmSettings(properties,pathString))
                    {
                        frmSet.ShowDialog();
                    }
                else
                {
                    MessageBox.Show("Devices not loaded, please check your Token Key.", "Devices not loaded",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return isReloaded;
                }
            }

            try
            {
                //_acceptCert = Settings.Default.AcceptCerts;
                _acceptCert = properties.AcceptCerts;
                //_devices = BulletAPI.BulletAPI.GetDevices(_acceptCert, Settings.Default.ApiKey);
                _devices = BulletAPI.BulletAPI.GetDevices(_acceptCert, properties.ApiKey);
                if (_devices.Count > 0)
                {
                    cmbDevices.DataSource = new BindingSource(_devices, null);
                    cmbDevices.DisplayMember = "Value";
                    cmbDevices.ValueMember = "Key";

                    cmbDevices.SelectedItem = cmbDevices.Items[0];
                    _idDev = cmbDevices.SelectedValue.ToString();
                    isReloaded = true;
                    manageRemindersToolStripMenuItem.Enabled = true;
                }
                else
                {
                    MessageBox.Show("No devices loaded, please add one device \r\n" +
                                    "on Pushbullet web site...", "No devices to send", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    isReloaded = false;
                }
                
                //Load contacts
                //_contacts = BulletAPI.BulletAPI.GetContacts(_acceptCert, Settings.Default.ApiKey);
                _contacts = BulletAPI.BulletAPI.GetContacts(_acceptCert, properties.ApiKey);
                if (_contacts.Count > 0)
                {
                    cmbContacts.Enabled = true;
                    chkSendToFriend.Enabled = true;
                    cmbContacts.DataSource = new BindingSource(_contacts, null);
                    cmbContacts.DisplayMember = "Key";
                    cmbContacts.ValueMember = "Value";

                    cmbContacts.SelectedItem = cmbContacts.Items[0];
                    _emailContact = cmbContacts.SelectedValue.ToString();
                }
                else
                {
                    cmbContacts.Enabled = false;
                    chkSendToFriend.Enabled = false;
                }

                if ((this.webSocketClient.State == WebSocketState.Initialized) || (this.webSocketClient.State == WebSocketState.Disconnected))
                webSocketClient.Connect("wss://stream.pushbullet.com/websocket/" + properties.ApiKey);
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException.Message == "Response status code does not indicate success: 401 (Unauthorized).")
                {
                    MessageBox.Show("Please check your API Key...", "Unauthorized Key.", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                    isReloaded = false;
                }
                else
                {
                    MessageBox.Show("Ups! Something went wrong, please check the log... \r\n"+ex.Message,"Error...",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                    isReloaded = false;
                }
            }
            catch (BulletAPI.ApiKeyNotFoundException ex)
            {
                MessageBox.Show("Token Key not loaded, please set your Token Key.", "Token Key not loaded",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                LogErrors.LogError(ex.Message + " \r\n" + ex.StackTrace);
                isReloaded = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ups! Something went wrong...");
                var cError = ex.Message + "\r\n" + ex.StackTrace;
                LogErrors.LogError(cError);
                isReloaded = false;
            }
            return isReloaded;
        }

        private void btnPushNote_Click(object sender, EventArgs e)
        {
            if (_sendToEmail)
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "note", _idDev, txtNotes.Text, txtTitleNote.Text,_emailContact);
            }
            else
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "note", _idDev, txtNotes.Text, txtTitleNote.Text);
            }
            tsMessage.Text = "Note Pushed!";
            txtTitleNote.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }

        private void btnPushLink_Click(object sender, EventArgs e)
        {
            if (_sendToEmail)
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "link", _idDev, txtLink.Text.Trim(),
                    txtTitleLink.Text, _emailContact);
            }
            else
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "link", _idDev, txtLink.Text.Trim(), txtTitleLink.Text);
            }
            tsMessage.Text = "Link Pushed!";
            txtLink.Text = string.Empty;
        }

        private void btnFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            txtFile.Text = openFileDialog.FileName;

            if (_sendToEmail)
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "file", _idDev, "", "", _emailContact, openFileDialog.FileName);
            }
            else
            {
                BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "file", _idDev, "", "", "", openFileDialog.FileName);
            }
            tsMessage.Text = "File Pushed!";
            txtFile.Text = string.Empty;
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            ChangeSize();
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            //orig size 393; 414 
            this.Size = new Size(393, 264);
        }

        private void tabPage5_Enter(object sender, EventArgs e)
        {
            this.Size = new Size(393, 264);
        }
        
        private void ChangeSize()
        {
            this.Size = new Size(393, 414);
        }

        private void sToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fSettings = new frmSettings(properties,pathString);
            fSettings.ShowDialog();
            Cursor.Current = Cursors.WaitCursor;
            if (LoadDevices())
            {
                properties = fSettings.Properties;
                EnablePush();
                enableReminders();
            }   
            Cursor.Current = Cursors.Default;
        }
        
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bullet Sharp. V"+ _version + _build+" (2014-2015) Released under GPL License and use" +
                            " JDI WebSocket Client that is under Apache 2.0 License." +
                            " \r\n Bullet Sharp Developed by Max J. Rodríguez Beltrán.\r\n" +
                            " This app use BulletAPI(Library to use Pushbullet from C#) " +
                            "Fork this app: https://bitbucket.org/Jaxmetalmax/bulletsharp", "Bullet Sharp.",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void reloadDevicesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (LoadDevices())
            {
                MessageBox.Show("Devices Reloaded!");
                Cursor.Current = Cursors.Default;
                EnablePush();
                enableReminders();
            }
            else
            {
                DisablePush();
            }
        }

        private void cmbDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            _idDev = cmbDevices.SelectedValue.ToString();
        }

        private void DisablePush()
        {
            if (properties.ApiKey != "") return;


            foreach (Control btnControl in this.Controls)
            {
                foreach (Control control in this.Controls)
                {
                    EnaButton(control, false);
                }
            }
        }

        private void EnablePush()
        {
            foreach (Control control in this.Controls)
            {
                EnaButton(control,true);
            }
        }

        private void EnaButton(Control con, bool enable)
        {
            if (con is Button)
                con.Enabled = enable;
            else if (con.HasChildren)
            {
                 foreach (Control ctrl in con.Controls)
                {
                    EnaButton(ctrl,enable);
                }
            }   
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void manageRemindersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmReminders rem = new frmReminders(_devices);
            rem.ShowDialog();
            loadReminders();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            loadReminders();
        }

        private void loadReminders()
        {
            _dtReminderes = Helper.loadReminders();
        }

        private void sendReminder()
        {
            foreach (DataRow row in _dtReminderes.Rows)
            {
                var date = Convert.ToDateTime(row["Date"].ToString() + " " + row["Time"].ToString());

                if ((date <= DateTime.Now) && row["Sent"].ToString() == "0")
                {
                    try
                    {
                        BulletAPI.BulletAPI.PushIt(_acceptCert, properties.ApiKey, "note", row["Id Device"].ToString(), row["reminder"].ToString());
                        Helper.UpdateReminder(row["Id"].ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ups! Something went wrong with reminders");
                        var cError = ex.Message + "\r\n" + ex.StackTrace;
                        LogErrors.LogError(cError);
                    }
                }
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            sendReminder();
        }

        private void enableReminders()
        {
            loadReminders();
            timer1.Enabled = true;
            timer1.Start();
            timer2.Enabled = true;
            timer2.Start();
            sendReminder();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized != this.WindowState) return;
            notifyIcon1.Visible = true;
            this.ShowInTaskbar = false;
            notifyIcon1.BalloonTipTitle = "Bullet Sharp";
            notifyIcon1.BalloonTipText = "Bullet Sharp will be \r\n running in system tray";
            notifyIcon1.ShowBalloonTip(5000);
            
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void notifyIcon1_MouseMove(object sender, MouseEventArgs e)
        {
            //notifyIcon1.ShowBalloonTip(1000);
            //notifyIcon1.BalloonTipText = "Bullet Sharp";
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.webSocketClient.State != WebSocketState.Initialized &&
                this.webSocketClient.State != WebSocketState.Disconnected)
            {
                this.webSocketClient.Disconnect();
            }
        }

        private void txtLink_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.webSocketClient != null)
            {
                this.webSocketClient.Dispose();
            }
            this.webSocketClient = null;
        }

        private void chkSendToFriend_CheckedChanged(object sender, EventArgs e)
        {
            _sendToEmail = chkSendToFriend.Checked;
        }

        private void cmbContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            _emailContact = cmbContacts.SelectedValue.ToString();
        }
    }
}
